import React from "react";

import Header from "../Components/Header";

function ProfilePage() {
  return (
    <React.Fragment>
      <Header backButton="/"></Header>
      <h1>I am Profile Page</h1>
    </React.Fragment>
  );
}

export default ProfilePage;
