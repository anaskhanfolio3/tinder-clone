import React from "react";
import SwipeButtons from "../Components/SwipeButtons";
import TinderCards from "../Components/TinderCards";
import Header from "../Components/Header";

function HomePage() {
  return (
    <React.Fragment>
      <Header></Header>
      <TinderCards />
      <SwipeButtons />
    </React.Fragment>
  );
}

export default HomePage;
