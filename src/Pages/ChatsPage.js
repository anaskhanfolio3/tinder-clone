import React from "react";
import Header from "../Components/Header";
import Chats from "../Components/Chats";

function ChatsPage() {
  return (
    <React.Fragment>
      <Header backButton="/"></Header>
      <Chats></Chats>
    </React.Fragment>
  );
}

export default ChatsPage;
