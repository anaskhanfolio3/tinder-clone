import React from "react";
import Header from "../Components/Header";
import ChatScreen from "../Components/ChatScreen";

function MessagesPage() {
  return (
    <React.Fragment>
      <Header backButton="/chats"></Header>
      <ChatScreen></ChatScreen>
    </React.Fragment>
  );
}

export default MessagesPage;
