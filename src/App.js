import Header from "./Components/Header";
import { BrowserRouter as Router, Routes, Route } from "react-router-dom";
import HomePage from "./Pages/HomePage";
import ChatsPage from "./Pages/ChatsPage";
import ProfilePage from "./Pages/ProfilePage";
import MessagesPage from "./Pages/MessagesPage";

function App() {
  return (
    <div className="App">
      <Router>
        <Routes>
          <Route path="/" element={<HomePage />}></Route>

          <Route path="/chats" element={<ChatsPage />}></Route>

          <Route path="/profile" element={<ProfilePage />}></Route>

          <Route path="/chats/:person" element={<MessagesPage />}></Route>
        </Routes>
      </Router>
    </div>
  );
}

export default App;
