import { initializeApp } from "firebase/app";
import { getFirestore } from "firebase/firestore/lite";

const firebaseConfig = {
  apiKey: "AIzaSyBSvygmcqQU8Yln7IUmd1HjmBRqfxex5Rc",
  authDomain: "tinder-clone-cf81a.firebaseapp.com",
  projectId: "tinder-clone-cf81a",
  storageBucket: "tinder-clone-cf81a.appspot.com",
  messagingSenderId: "140864631510",
  appId: "1:140864631510:web:2adb93e53a9514fd3ccf77",
  measurementId: "G-GH7BQ5ZL8J",
};

const app = initializeApp(firebaseConfig);
const db = getFirestore(app);

export default db;
