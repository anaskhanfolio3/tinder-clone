import React from "react";
import "./ChatsRow.css";
import ChatRow from "./ChatsRow";

function Chats() {
  return (
    <div className="chats">
      <ChatRow
        name="Sarah"
        message="Hey! How are you"
        timestamp="35 minutes a go"
        profilePic="https://picsum.photos/id/237/200/200"
      ></ChatRow>
      <ChatRow
        name="Ellen"
        message="Whats up?"
        timestamp="55 minutes a go"
        profilePic="https://picsum.photos/id/238/200/200"
      ></ChatRow>
      <ChatRow
        name="Sandra"
        message="Ola!"
        timestamp="3 days a go"
        profilePic="https://picsum.photos/id/239/200/200"
      ></ChatRow>
      <ChatRow
        name="Natasha"
        message="Who is this?"
        timestamp="1 week a go"
        profilePic="https://picsum.photos/id/240/200/200"
      ></ChatRow>
    </div>
  );
}

export default Chats;
