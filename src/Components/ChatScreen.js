import React, { useState } from "react";
import "./ChatScreen.css";
import Avatar from "@mui/material/Avatar";

function ChatScreen() {
  const [input, setInput] = useState("");
  const [messages, setMessages] = useState([
    {
      name: "Ellen",
      image: "...",
      message: "Whats up?",
    },
    {
      name: "Ellen",
      image: "...",
      message: "How it's going?",
    },
    {
      message: "Hi, how are you?",
    },
  ]);

  function handleSend(e) {
    e.preventDefault();
    if (input === "") return;

    setMessages([...messages, { message: input }]);
    setInput("");
  }
  return (
    <div className="chatScreen">
      <p className="chatScreen__timestamp">
        YOU MATCHED WITH ELLEN ON 10/08/20
      </p>
      {messages.map((message) =>
        message.name ? (
          <div className="chatScreen__message">
            <Avatar className="chatScreen__image" src={message.image}></Avatar>
            <p className="chatScreen__text">{message.message}</p>
          </div>
        ) : (
          <div className="chatScreen__message">
            <p className="chatScreen__textUser">{message.message}</p>
          </div>
        )
      )}
      <form className="chatScreen__input">
        <input
          className="chatScreen__inputField"
          type="text"
          placeholder="Type a messsage..."
          value={input}
          onChange={(e) => setInput(e.target.value)}
        ></input>
        <button
          className="chatScreen__inputButton"
          type="submit"
          onClick={handleSend}
        >
          Send
        </button>
      </form>
    </div>
  );
}

export default ChatScreen;
