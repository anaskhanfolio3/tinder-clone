import React, { useState, useEffect } from "react";
import TinderCard from "react-tinder-card";
import "./TinderCards.css";
import db from "../Firebase";
import { collection, getDocs } from "firebase/firestore/lite";

function TinderCards() {
  const [people, setPeople] = useState([]);

  useEffect(() => {
    fetchAndUpdatePeople(db);
  }, []);

  async function fetchAndUpdatePeople(database) {
    const peopleCol = collection(database, "people");
    const peopleSnapshot = await getDocs(peopleCol);
    const peopleList = peopleSnapshot.docs.map((doc) => doc.data());
    setPeople(peopleList);
  }

  return (
    <div className="tinderCards__cardContainer">
      {people.map((person) => (
        <TinderCard
          className="swipe"
          key={person.name}
          preventSwipe={["up", "down"]}
        >
          <div
            style={{ backgroundImage: `url(${person.url})` }}
            className="card"
          >
            <h3>{person.name}</h3>
          </div>
        </TinderCard>
      ))}
    </div>
  );
}

export default TinderCards;
