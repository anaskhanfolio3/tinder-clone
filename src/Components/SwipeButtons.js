import "./SwipeButtons.css";
import ReplayIcon from "@mui/icons-material/Replay";
import CloseIcon from "@mui/icons-material/Close";
import StarRateIcon from "@mui/icons-material/StarRate";
import FavoriteIcon from "@mui/icons-material/Favorite";
import BoltIcon from "@mui/icons-material/Bolt";
import { IconButton } from "@mui/material";

function SwipeButtons() {
  return (
    <div className="swipeButtons">
      <IconButton>
        <ReplayIcon className="swipeButtons__repeat" fontSize="large" />
      </IconButton>

      <IconButton>
        <CloseIcon className="swipeButtons__left" fontSize="large" />
      </IconButton>

      <IconButton>
        <StarRateIcon className="swipeButtons__star" fontSize="large" />
      </IconButton>

      <IconButton>
        <FavoriteIcon className="swipeButtons__right" fontSize="large" />
      </IconButton>

      <IconButton>
        <BoltIcon className="swipeButtons__bolt" fontSize="large" />
      </IconButton>
    </div>
  );
}

export default SwipeButtons;
