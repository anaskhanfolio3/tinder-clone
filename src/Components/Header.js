import React from "react";
import "./Header.css";
import tinderlogo from "../tinderlogo.png";
import PersonIcon from "@mui/icons-material/Person";
import ChatIcon from "@mui/icons-material/Chat";
import BackIcon from "@mui/icons-material/ArrowBackIos";
import IconButton from "@mui/material/IconButton";
import { Link, useNavigate } from "react-router-dom";

function Header({ backButton }) {
  const navigate = useNavigate();
  return (
    <div className="header">
      {backButton ? (
        <IconButton
          onClick={() => {
            navigate(backButton);
          }}
        >
          <BackIcon className="header__icon" fontSize="large" />
        </IconButton>
      ) : (
        <Link to="/profile">
          <IconButton>
            <PersonIcon className="header__icon" fontSize="large" />
          </IconButton>
        </Link>
      )}

      <Link to="/">
        <img className="header__logo" src={tinderlogo} alt=""></img>
      </Link>
      <Link to="/chats">
        <IconButton>
          <ChatIcon className="header__icon" fontSize="large" />
        </IconButton>
      </Link>
    </div>
  );
}

export default Header;
